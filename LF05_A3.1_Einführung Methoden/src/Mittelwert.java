
public class Mittelwert {

	static double berechneMittelwert(double x, double y) {
		return (x + y)/2.0;
	}
   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte fuer x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 6.0;
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = (x + y) / 2.0;
      
      //Methode aufrufen/Werte angeben
      
      double a= 2.0;
      double b= 6.0;
      
      double mittelwert = berechneMittelwert(a,b);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
      //Methode ausgeben
      System.out.printf("Der Mittelwert ist %.2f", mittelwert);
   }
}
