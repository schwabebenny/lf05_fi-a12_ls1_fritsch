/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Benjamin Fritsch >>
  */
public class WeltDerZahlen {

	public static void main(String[] args) {
	    /*  *********************************************************
	    
        Zuerst werden die Variablen mit den Werten festgelegt!
   
   *********************************************************** */
   // Im Internet gefunden ?
   // Die Anzahl der Planeten in unserem Sonnesystem                    
   int anzahlPlaneten =  8;
   
   // Anzahl der Sterne in unserer Milchstra�e
   long  anzahlSterne = 45000000000l;
   
   // Wie viele Einwohner hat Berlin?
   int   bewohnerBerlin =  3766082;
   
   // Wie alt bist du?  Wie viele Tage sind das?
   
   int   alterTage = 9192;
   
   // Wie viel wiegt das schwerste Tier der Welt?
   // Schreiben Sie das Gewicht in Kilogramm auf!
   int    gewichtKilogramm =   200000;
   
   // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
   int   flaecheGroessteLand = 17098242;
   
   // Wie gro� ist das kleinste Land der Erde?
   
   int  flaecheKleinsteLand = 1;
   
   
   
   
   /*  *********************************************************
   
        Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
   
   *********************************************************** */
   
   System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
   
   System.out.println("Anzahl der Bewohner in Berlin: " + bewohnerBerlin);
   
   
   System.out.println("Anzhahl der Tage meines Alters: " + alterTage);
   
   
   System.out.println("Schwerste Tier der Welt in Kilogramm: " + gewichtKilogramm);
   
   System.out.println("Das gr��te Land in km�: " + flaecheGroessteLand );
   
   System.out.println("Das kleinste Land in km�: " + flaecheKleinsteLand);
   
   
   System.out.println(" *******  Ende des Programms  ******* ");

	}

}
