import java.util.ArrayList;

public class Raumschiff_test {

    public static void main(String[] args) {
        Raumschiff klingonen = new Raumschiff("IKS Hegh'ta");
        klingonen.setAndroidenAnzahl(2); klingonen.setPhotonentorpedoAnzahl(3);
        Ladung ferengi = new Ladung("Ferengi Schneckensaft", 200); 
        klingonen.addLadung(ferengi);
        Ladung schwert = new Ladung("Bet'leth Klingonen Schwert", 200); 
        klingonen.addLadung(schwert);
        
        Raumschiff vulkanier = new Raumschiff("N'Var", 5, 0);
        Ladung forschungssonde = new Ladung("Forschungssonde", 35); 
        vulkanier.addLadung(forschungssonde);

        Raumschiff romulaner = new Raumschiff("IRW Khazara", 2);
        romulaner.setPhotonentorpedoAnzahl(2); 
        romulaner.setSchildeInProzent(200);
        Ladung borg = new Ladung("Borg-Schrott", 5); 
        romulaner.addLadung(borg);
        Ladung rote = new Ladung("Rote Materie", 2); 
        romulaner.addLadung(rote);
        Ladung plasma = new Ladung("Plasma Waffe", 50); 
        romulaner.addLadung(plasma);



        klingonen.photonenTorpedosAbschießen(1); romulaner.trefferVermerken(1);

        romulaner.phaserKanoneAbschießen(); klingonen.trefferVermerken(1);

        vulkanier.nachrichtenAnAlle("Gewalt ist nicht logisch");

        klingonen.zustandvomRaumschiffsausgeben();
        klingonen.ausgabeladungsverzeichnis();
        vulkanier.reparaturAndroidenEinsetzen(5, true, true, true);
        Ladung l3_2 = new Ladung("Photonentorpedo", 3);
        vulkanier.addLadung(l3_2); vulkanier.torpedoverladen(3); //l3_2.ladungAufräumen();
        klingonen.photonenTorpedosAbschießen(2); romulaner.trefferVermerken(2);

        klingonen.zustandvomRaumschiffsausgeben();
        klingonen.logBucheintraegezurueckgeben();
        klingonen.ausgabeladungsverzeichnis();

        romulaner.zustandvomRaumschiffsausgeben();
        romulaner.logBucheintraegezurueckgeben();
        romulaner.ausgabeladungsverzeichnis();

        vulkanier.zustandvomRaumschiffsausgeben();
        vulkanier.logBucheintraegezurueckgeben();
        vulkanier.ausgabeladungsverzeichnis();
    }
}