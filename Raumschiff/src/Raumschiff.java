import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * 
 * @author Benny
 * @version 4
 *
 */
public class Raumschiff {
	private Ladung ladung;
	private ArrayList<Ladung> ladungen = new ArrayList<Ladung>();
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleinProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
    private String logBuch[] = new String[100];
	ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	/**
	 * 
	 * @param schiffsname
	 */
    public Raumschiff(String schiffsname) {
        this.schiffsname = schiffsname;
    }
    /**Constructor*/
    public Raumschiff (String schiffsname, int androidenAnzahl) {
        this.schiffsname = schiffsname;
        this.androidenAnzahl = androidenAnzahl;
        this.photonentorpedoAnzahl = 0;
    }

    /**Constructor*/
    public Raumschiff (String schiffsname, int androidenAnzahl, int photonenTorpedoAnzahl) {
        this.schiffsname = schiffsname;
        this.androidenAnzahl = androidenAnzahl;
        this.photonentorpedoAnzahl = photonenTorpedoAnzahl;
    }

    /**
     * 
     * @param photonentorpedoAnzahl
     * @param energiversorgungInProzenzt
     * @param schildeInProzent
     * @param huelleinProzent
     * @param lebenserhaltungssystemeInProzent
     * @param androidenAnzahl
     * @param schiffsname
     * @param broadcastKommunikator
     * @param ladungsverzeichnis
     */
	public void neuesRaumschiff(int photonentorpedoAnzahl, int energiversorgungInProzenzt, int schildeInProzent,
			int huelleinProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname,
			ArrayList<String> broadcastKommunikator, ArrayList<Ladung> ladungsverzeichnis) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energiversorgungInProzenzt;
		this.schildeInProzent = schildeInProzent;
		this.huelleinProzent = huelleinProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.broadcastKommunikator = broadcastKommunikator;
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	/**
	 * 
	 * @return
	 */
    public int getEnergieversorgungInProzent() {
        return this.energieversorgungInProzent;
    }
    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }


	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleinProzent() {
		return huelleinProzent;
	}

	public void setHuelleinProzent(int huelleinProzent) {
		this.huelleinProzent = huelleinProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	
	
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}


	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	/**
	 * 
	 * @param ladung
	 */
	  public void addLadung(Ladung ladung) {
	       this.ladungsverzeichnis.add(ladung);
	    }
	    public void zustandvomRaumschiffsausgeben() {
	        System.out.printf("Zustand vom Raumschiff [%s]\n " +
	                        "Photonentorpedos: %d \n " +
	                        "Energieversorgung: %d%% \n " +
	                        "Schilde: %d%% \n " +
	                        "Hülle: %d%% \n " +
	                        "Lebenserhaltungssysteme: %d%% \n " +
	                        "Androiden: %d",
	                this.schiffsname, this.photonentorpedoAnzahl, this.energieversorgungInProzent, this.schildeInProzent, this.huelleinProzent, this.lebenserhaltungssystemeInProzent, this.androidenAnzahl);
	    }

	    public void ausgabeladungsverzeichnis() {
	        System.out.printf("\n\nFolgende Objekte sind im Raumschiff [%s] geladen:\n", this.schiffsname);
	        for (Ladung element : ladungsverzeichnis) {
	            System.out.print(" " + element.getBezeichnung() + " (" + element.getMenge() + ") // [" + element + "]");
	        }
	    }
	    /**
	     * 
	     * @param anzahl
	     */
	    public void torpedoverladen(int anzahl) {
	        for (Ladung element : ladungen) {
	            if (element.getBezeichnung() == "Photonentorpedo") {
	                if (element.getMenge() < anzahl) {
	                    this.photonentorpedoAnzahl += element.getMenge();
	                    element.setMenge(0);
	                    ladungen.remove(element);
	                } else {
	                    this.photonentorpedoAnzahl += anzahl;
	                    element.setMenge(element.getMenge() - anzahl);
	                }
	            }
	        }
	    }
	    /**
	     * 
	     * @param anzahl
	     */
	    public void photonenTorpedosAbschießen(int anzahl) {
	        if (photonentorpedoAnzahl < anzahl) {
	            photonentorpedoAnzahl -= this.photonentorpedoAnzahl;
	            System.out.printf("\n\nPhotonentorpedo abgeschossen!");
	            System.out.printf("\n\nKonsole: (%d) Photonentorpedo(s) eingesetzt", this.photonentorpedoAnzahl);
	            torpedoverladen(this.photonentorpedoAnzahl);
	        } else if (this.photonentorpedoAnzahl == 0) {
	            System.out.printf("\nKonsole: Keine Photonentorpedos gefunden!");
	            System.out.printf("\n\n-=*Click*=-");
	            torpedoverladen(anzahl);
	        } else {
	            photonentorpedoAnzahl -= anzahl;
	            System.out.printf("\n\nPhotonentorpedo abgeschossen!");
	            System.out.printf("\n\nKonsole: (%d) Photonentorpedo(s) eingesetzt", anzahl);
	            torpedoverladen(anzahl);
	        }
	        logBuchEintrag("Photonentorpedo wurde abgeschossen");
	    }

	    public void phaserKanoneAbschießen() {
	        if (photonentorpedoAnzahl >= 50) {
	        	photonentorpedoAnzahl -= 50;
	            System.out.printf("\n\nPhaserkanone abgeschossen!");
	        } else {
	            System.out.printf("\n\n-=*Click*=-");
	        }
	    }
	    /**
	     * 
	     * @param anzahl
	     */
	    public void trefferVermerken(int anzahl) {
	        System.out.printf("\n\n[%s] wurde getroffen!", this.schiffsname);

	        if (this.schildeInProzent > 50*anzahl) {
	            this.schildeInProzent -= 50*anzahl;
	        } else if (this.schildeInProzent <= 50*anzahl) {
	            this.schildeInProzent -= this.schildeInProzent;

	            this.huelleinProzent -= 50*anzahl;
	            if (this.energieversorgungInProzent > 50*anzahl) {
	                this.energieversorgungInProzent -= 50*anzahl;
	            } else {
	                this.energieversorgungInProzent -= this.energieversorgungInProzent;
	            }
	        } else {
	            if (this.huelleinProzent <= 50*anzahl) {
	                this.huelleinProzent -= this.huelleinProzent;
	                this.lebenserhaltungssystemeInProzent -= this.lebenserhaltungssystemeInProzent;
	                System.out.printf("Lebenserhaltungssystem vom Raumschiff [%s] wurden zerstört!", this.schiffsname);
	            }
	        }
	        logBuchEintrag("Von Gegner getroffen");
	    }
	    /**
	     * 
	     * @param nachricht
	     */
	    public void nachrichtenAnAlle(String nachricht) {
	        System.out.printf("\n\n%s: %s", this.schiffsname, nachricht);
	    }
	    public void logBucheintraegezurueckgeben() {
	        System.out.printf("\nLogbuch Einträge");
	        for (int i = 1; i < this.logBuch.length && this.logBuch[i] != null; i++) {
	            System.out.printf("\n");
	            System.out.print(this.logBuch[i]);
	        }
	    }
	    /**
	     * 
	     * @param input
	     */
	    private void logBuchEintrag(String input) {
	        Date getDate = new Date();
	        SimpleDateFormat formatter = new SimpleDateFormat(" [dd-MM-yyyy | HH:mm:ss] ");
	        String date = formatter.format(getDate);
	        int c = 1;
	        while (c < this.logBuch.length-1) {
	            if (this.logBuch[c] == null) {
	                this.logBuch[c] = date + input;
	                break;
	            } else {
	                c++;
	            }
	        }
	    }
	    /**
	     * 
	     * @param anzahl
	     * @param energieversorgung
	     * @param huelle
	     * @param schilde
	     */
	    public void reparaturAndroidenEinsetzen(int anzahl, boolean energieversorgung, boolean huelle, boolean schilde) {
	        Random rand = new Random(); int n = rand.nextInt(100); n += 1;
	        int strukturen = 0; if (energieversorgung) {strukturen++;} if (huelle) {strukturen++;} if (schilde) {strukturen++;}
	        if (anzahl > this.androidenAnzahl) {
	            float ergebnis = n*this.androidenAnzahl/strukturen;
	            if (energieversorgung) {this.energieversorgungInProzent += ergebnis;} if (huelle) {this.huelleinProzent += ergebnis;} if (schilde) {this.schildeInProzent += ergebnis;}
	        } else {
	            float ergebnis = n*anzahl/strukturen;
	            if (energieversorgung) {this.energieversorgungInProzent += ergebnis;} if (huelle) {this.huelleinProzent += ergebnis;} if (schilde) {this.schildeInProzent += ergebnis;}
	        }
	    }
	
}
