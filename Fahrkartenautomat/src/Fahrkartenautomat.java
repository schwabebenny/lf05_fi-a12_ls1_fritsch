﻿import java.util.Scanner;

public class Fahrkartenautomat {
	public static double fahrkartenbestellungErfassen (String text) {
		System.out.println(text);
		double preis [] = {
				0.00,
	 			2.90,
	 			8.60,
	 			23.50
	 	};
	 	String ticket [] = {
	 			"Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:",
	 			"Einzelfahrschein Regeltarif AB [2,90 EUR] (1)",
	 			"Tageskarte Regeltarif AB [8,60 EUR] (2)",
	 			"Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)"
	 	};
	 	
	 	for(int i=0; i <ticket.length; i++) {
	 		System.out.println(ticket[i]);
	 		
	 	};
	 		
	 		Scanner tastatur = new Scanner(System.in);
	 		int ticketwahl = tastatur.nextInt();
	 		System.out.println("Ihre Wahl " + ticketwahl);
	 		System.out.println("Bitte Anzahl der Tickets eingeben");
	 		int anzahlticket = tastatur.nextInt();
	 		System.out.println("Anzahl der Tickets " + anzahlticket);
	 		 if(anzahlticket> 10) {
	 			 System.out.println("Die Anzahl der max. Tickets beträgt 10 u. wird als 1 Ticket gewertet");
	 			 double zuZahlenderBetrag = 1 * preis[ticketwahl];
	 			 return zuZahlenderBetrag;
	 		 }else if (anzahlticket < 0) {
	 			System.out.println("Die Anzahl der Tickets kann nicht kleiner als 0 sein u. wird als 1 Ticket gewertet");
	 			 double zuZahlenderBetrag = 1 * preis[ticketwahl];
	 			 return zuZahlenderBetrag;
	 		 }else {
	 			 double zuZahlenderBetrag = anzahlticket * preis[ticketwahl];
	 			 
	 			 return zuZahlenderBetrag;
	 		 }
	 		 
	 	
	}
public static double fahrkartenBezahlen(String Text, double zuZahlenderBetrag) {
	double eingezahlterGesamtbetrag = 0.0;
       double eingeworfeneMünze;
       System.out.println(Text);
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    		Scanner tastatur = new Scanner(System.in);
    	   System.out.printf("Noch zu zahlen: %.2f Euro%n",(zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
           
           
       }
	return eingezahlterGesamtbetrag;
}
public static void warte(int millisekunden) {
	try {
		Thread.sleep(millisekunden);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return;
}
public static void muenzeAusgeben(int betrag, String einheit) {
	return;
}
public static double fahrkartenausgabe(String text) {
	
	System.out.println(text);
	for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          warte(250);
       }
       System.out.println("\n\n");
	return 0;
}
public static double rueckgeldausgabe(String text) {
	double rückgabebetrag = 0;
	System.out.println(text);
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
           while(rückgabebetrag <= 0) {
        	   System.out.println("Rückgabetrag beträgt " + rückgabebetrag);
           }
       }
	return rückgabebetrag;
}
public static void main(String[] args) {
	while(true) {
		double zuZahlenderBetrag = fahrkartenbestellungErfassen("Fahrkartenbestellvorgang:\r\n" + 
		"=========================\n");

double betrag = fahrkartenBezahlen("====Betrag====", zuZahlenderBetrag);

double rueckgabebetrag = rueckgeldausgabe("==Rückgeldbetrag==");

double ausgabe = fahrkartenausgabe("\nFahrschein wird ausgegeben");

// Rückgeldberechnung und -Ausgabe
// -------------------------------


System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                  "vor Fahrtantritt entwerten zu lassen!\n"+
                  "Wir wünschen Ihnen eine gute Fahrt.");
	}

}
}

